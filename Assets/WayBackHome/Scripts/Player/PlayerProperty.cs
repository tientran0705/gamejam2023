using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProperty : NetworkBehaviour
{
    public float maxHP;
    public float currentHPValue;

    [Networked] private TickTimer tickDamage { get; set; }
    [Networked] public byte HPRate { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void FixedUpdateNetwork()
    {
        
    }
}
