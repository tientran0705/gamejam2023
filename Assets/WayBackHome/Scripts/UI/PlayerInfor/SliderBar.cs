using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderBar : MonoBehaviour
{
    public Slider BarSld;
    public Gradient gradient;
    public Image fill;


    public void SetMaxValue(int maxValue)
    {
        BarSld.maxValue = maxValue;
        BarSld.value = maxValue;
        gradient.Evaluate(1f);
    }

    public void SetMaxValueWithoutStart(int maxValue, int currentValue)
    {
        BarSld.maxValue = maxValue;
        BarSld.value = currentValue;
        gradient.Evaluate(1f);
    }

    public void SetValueBar(int currentValue)
    {
        BarSld.value = currentValue;
        fill.color = gradient.Evaluate(BarSld.normalizedValue);
    }

    public void AddValueBar(int valueAdd)
    {
        BarSld.value += valueAdd;
        fill.color = gradient.Evaluate(BarSld.normalizedValue);
    }

    public void SubValueBar(int valueSub)
    {
        BarSld.value -= valueSub;
        fill.color = gradient.Evaluate(BarSld.normalizedValue);
    }


}
