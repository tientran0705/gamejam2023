using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedometer : MonoBehaviour
{
    private const float MAX_SPEED_ANGLE = -60;
    private const float ZERO_SPEED_ANGLE = 60;

    public Transform needleTransform;

    private float speedMax;
    private float speed;
    private float currentSpeed;

    private void Awake()
    {
        //needleTransform = transform.Find("needle");

        speed = 0f;
        speedMax = 5000f;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        InitSpeed();
    }

    private void InitSpeed()
    {
        if(currentSpeed > speedMax)
        {
            currentSpeed = speedMax;
        }
        if(speed < currentSpeed)
        {
            speed += 500f * Time.deltaTime;
        }
        else if(speed > currentSpeed)
        {
            speed -= 500f * Time.deltaTime;
        }
        needleTransform.eulerAngles = new Vector3(0, 0, GetSpeedRotation());
    }

    public void SetCurrentSpeed(int speed)
    {
        currentSpeed = speed;
    }

    private float GetSpeedRotation()
    {
        float totalAngleSize = ZERO_SPEED_ANGLE - MAX_SPEED_ANGLE;
        float speedNormalized = speed / speedMax;
        return ZERO_SPEED_ANGLE - speedNormalized * totalAngleSize;
    }
}
