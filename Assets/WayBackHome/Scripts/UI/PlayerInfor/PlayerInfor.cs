using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfor : PlayerInforBase
{
    public Text PlayerNameTxt;
    public SliderBar Fuel;
    public SliderBar Armor;
    public Speedometer SpeedometerMotor;
}
