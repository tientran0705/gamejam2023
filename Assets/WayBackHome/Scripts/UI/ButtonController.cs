using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonController : MonoBehaviour
{
    [SerializeField] List<ButtonAction> buttonActions;


    private ButtonAction buttonJumpAction;


    public bool GetButtonAction(string id)
    {
        return buttonActions.Find(x => x.id == id).stateAction == ButtonSate.onPressed;
    }
}
