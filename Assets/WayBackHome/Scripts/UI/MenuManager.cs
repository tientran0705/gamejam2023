using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : Singleton<MenuManager>
{

    public void OnClickButtonChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
