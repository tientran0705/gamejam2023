using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] SliderBar engineLevel;
    [SerializeField] SliderBar tankerLevel;
    [SerializeField] SliderBar wheelLevel;
    [SerializeField] Text indexTxt;
    //[SerializeField] Text speed;
    [SerializeField] Player player;

    const int maxSld = 3;
    // Start is called before the first frame update
    void Start()
    {
        Init();
        //engineLevel.SetValueBar()
    }

    void Init()
    {
        player.LoadPlayer();
        engineLevel.SetMaxValueWithoutStart(maxSld, player.currentEngineSld);
        tankerLevel.SetMaxValueWithoutStart(maxSld, player.currentTankerSld);
        wheelLevel.SetMaxValueWithoutStart(maxSld, player.currentWheelSld);
        indexTxt.text = RunnerSaveController.Instance.GetIndexSpawnRunner().ToString();

    }

    // Update is called once per frame
    void Update()
    {
        // player.SavePlayer();
        
        //speed.text = player.health.ToString();
    }

    public void UpdateData() { }

    public void LoadLevel()
    {
        player.LoadPlayer();
        
    }

    public void SavePlayer()
    {
        player.ChangeHealth(10);
        player.SavePlayer();
    }

    public void OnClickLeftButton()
    {
        RunnerSaveController.Instance.SetIndexSpawnRunner(0);
        indexTxt.text = RunnerSaveController.Instance.GetIndexSpawnRunner().ToString() + 1;
    }

    public void OnClickRightButton()
    {
        RunnerSaveController.Instance.SetIndexSpawnRunner(1);
        indexTxt.text = RunnerSaveController.Instance.GetIndexSpawnRunner().ToString() + 1;
    }

    public void OnClickEngine()
    {
        player.ChangeSpeed(100);
        player.ChangeFuel(20);
        player.currentEngineSld += 1;
        engineLevel.SetValueBar(player.currentEngineSld);
        player.SavePlayer();
    }

    public void OnClickTanker()
    {
        player.ChangeFuel(40);
        player.currentTankerSld += 1;
        tankerLevel.SetValueBar(player.currentTankerSld);
        player.SavePlayer();
    }

    public void OnClickWheel()
    {
        player.ChangeHealth(100);
        player.currentWheelSld += 1;
        wheelLevel.SetValueBar(player.currentWheelSld);
        player.SavePlayer();
    }

    public void OnClickBack()
    {
        MenuManager.Instance.OnClickButtonChangeScene("Lobby");
    }
}
